<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model {

	public $id;
	public $name;
	public $price;
	public $qty;
	public $desc;

	public function get_last_ten()
	{
			$query = $this->db->get('produks', 10);
			return $query->result();
	}

	public function insert()
	{
			$this->id        = $_POST['id']; // please read the below note
			$this->name      = $_POST['name'];
			$this->price     = $_POST['price'];
			$this->qty       = $_POST['qty'];
			$this->desc      = $_POST['desc'];

			$this->db->insert('entries', $this);
	}

	public function update()
	{
			$this->id         = $_POST['id'];
			$this->name       = $_POST['name'];
			$this->price      = $_POST['price'];
			$this->qty        = $_POST['qty'];
			$this->desc       = $_POST['desc'];

			$this->db->update('entries', $this, array('id' => $_POST['id']));
	}

	public function delete()
	{
			$this->db->delete('users', array('id' =>  $_POST['id'])); 
	}

}

